﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.U2D;
using UnityEngine.UI;

public class UI_EmojiButton : MonoBehaviour
{
    public string m_unicodeHexaId = "";
    public RawImage m_displayImage;
    public Texture m_default;
    public Text m_text;
    public UnicodeDeleguateEvent m_listener;
    public UnicodeEvent m_onSelected;

    public void SetFromHexa(string hexaUnicode)
    {
        m_unicodeHexaId = hexaUnicode;
        Refresh();
    }

    public void NotifyAsSelected() {
            UnicodeId u = new UnicodeId();
            u.SetAsHexa(m_unicodeHexaId);
        m_onSelected.Invoke(u);
        if (m_listener != null) {
            m_listener(u);
        }
    }


    private void OnValidate()
    {
        if(Application.isPlaying)
        Refresh();

    }

    private Material m=null;
    private void Refresh()
    {
        //if (m == null)
        //    m = Material.Create(m_displayImage.materialForRendering);
        Texture image=null;
        string t;
        UnicodeUtility.TryToHaveSpriteAndTextOf(m_unicodeHexaId,ref image,out t);
        if (image != null)
        {
            m_displayImage.texture = image;
        }
        else { 
            m_displayImage.texture = m_default;
        }
        if (image == null)
        {
            m_text.text = t;
          
        }
        else m_text.text = "";
    }

    internal void RemoveListener(UnicodeDeleguateEvent value)
    {
        m_listener -= value;
    }

    internal void AddListener(UnicodeDeleguateEvent value)
    {
        m_listener += value;
    }

    public  void Clear()
    {
        m_text.text = "";
        m_displayImage.texture = m_default;
    }
}
public class UnicodeUtility
{
    public static void TryToHaveSpriteAndTextOf(string unicodeHexaId, ref Texture image, out string text)
    {
        TryToHaveSpriteOf(unicodeHexaId, ref image);
        TryToHaveTextOf(unicodeHexaId, out text);
    }   
    public static void TryToHaveSpriteOf(string unicodeHexaId, ref Texture image)
    {
        LoadUnicodeStoredInProject();
        image = null;
        unicodeHexaId = unicodeHexaId.ToLower().Trim();
        if (m_registre.ContainsKey(unicodeHexaId))
            image =m_registre[unicodeHexaId];
    }
    public static void TryToHaveTextOf(string unicodeHexaId, out string text)
    {
        try
        {
            int value = Convert.ToInt32(unicodeHexaId, 16);
           
                text = char.ConvertFromUtf32(value).ToString();
            if (text.Trim() == "" || text.Trim() == " ") {
                text = unicodeHexaId;
            } 

             //   text = string.Format("{0:X4}", value);
        }
        catch (Exception e) {
            //UnityEngine.Debug.Log("Hum:" + unicodeHexaId);
            text = "";
        }
    }
    public static void TryToHaveSpriteOf(UnicodeId unicode, ref Texture image)
    {
        TryToHaveSpriteOf(unicode.GetHexaAsString(), ref image);
    }
    public static void TryToHaveTextOf(UnicodeId unicode, out string text)
    {
        TryToHaveTextOf(unicode.GetHexaAsString(), out text);
    }

    public static Texture[] GetAllSpriteInResources()
    {
        LoadSprite();
        return m_sprites;
    }
    public static string[] GetAllUnicodeHexaNameInResources()
    {
        LoadSprite();
        return m_registeredList;

    }
    public static UnicodeId[] GetAllUnicodeInResources()
    {
        LoadSprite();
        List<UnicodeId> u = new List<UnicodeId>();
        for (int i = 0; i < m_registeredList.Length; i++)
        {
            u.Add(new UnicodeId(m_registeredList[i]));
        }
        return u.ToArray();
    }
    static ListOfUnicodeSprites m_atlas =null;
    static string[] m_registeredList = new string[0];
    static Dictionary<string, Texture> m_registre = new Dictionary<string, Texture>();
    static Texture[] m_sprites=new Texture[0];
    static void LoadSprite(bool refreshIfAlreadyLoad=false) {
        if (refreshIfAlreadyLoad || m_atlas == null) { 
        
            m_atlas = Resources.Load<ListOfUnicodeSprites>("UnicodeSpriteList");
            m_sprites = m_atlas.m_allSprites;
            m_registre.Clear();
            for (int i = 0; i < m_sprites.Length; i++)
            {
                m_registre.Add(m_sprites[i].name.Trim().ToLower(), m_sprites[i]);
            }
            m_registeredList = m_registre.Keys.ToArray();
            UnityEngine.Debug.Log("??" + m_sprites.Length);
        }
    } 

    public static void LoadUnicodeStoredInProject()
    {
        LoadSprite();
    }

    public static UnicodeId[] GetUnicodesFromString(string textWithAllowUnicode)
    {
        textWithAllowUnicode = textWithAllowUnicode.Replace(" ", "").Replace("\n", "").Replace("\r", "");
        string t16 = textWithAllowUnicode;

        //string utf8 = UnicodeUtility.UTF16ToUTF8(t16);
        //UnityEngine.Debug.Log(">>>" +utf8);

        List<UnicodeId > l = new List<UnicodeId>();
        for (int i = 0; i < t16.Length; i += Char.IsSurrogatePair(t16, i) ? 2 : 1)
        {
            int x =Char.ConvertToUtf32(t16, i);
            l.Add(new UnicodeId(x));
        }

        //for (int i = 0; i < textWithAllowUnicode.Length; i++)
        //{
        //    UnityEngine.Debug.Log(">" + char.IsSurrogatePair(textWithAllowUnicode, i));
        //    UnityEngine.Debug.Log(">>" + textWithAllowUnicode[i].ToString());

        //}

        return l.ToArray();
    }

    public static string GetFullCodePointAtIndex(string s, int idx) =>
    s.Substring(idx, char.IsSurrogatePair(s, idx) ? 2 : 1);


    private static string UTF16ToUTF8(string textWithAllowUnicode)
    {//https://stackoverflow.com/questions/6198744/convert-string-utf-16-to-utf-8-in-c-sharp
        /**************************************************************
         * Every .NET string will store text with the UTF16 encoding, *
         * known as Encoding.Unicode. Other encodings may exist as    *
         * Byte-Array or incorrectly stored with the UTF16 encoding.  *
         *                                                            *
         * UTF8 = 1 bytes per char                                    *
         *    ["100" for the ansi 'd']                                *
         *    ["206" and "186" for the russian 'κ']                   *
         *                                                            *
         * UTF16 = 2 bytes per char                                   *
         *    ["100, 0" for the ansi 'd']                             *
         *    ["186, 3" for the russian 'κ']                          *
         *                                                            *
         * UTF8 inside UTF16                                          *
         *    ["100, 0" for the ansi 'd']                             *
         *    ["206, 0" and "186, 0" for the russian 'κ']             *
         *                                                            *
         * We can use the convert encoding function to convert an     *
         * UTF16 Byte-Array to an UTF8 Byte-Array. When we use UTF8   *
         * encoding to string method now, we will get a UTF16 string. *
         *                                                            *
         * So we imitate UTF16 by filling the second byte of a char   *
         * with a 0 byte (binary 0) while creating the string.        *
         **************************************************************/

        // Storage for the UTF8 string
        string utf8String = String.Empty;

            // Get UTF16 bytes and convert UTF16 bytes to UTF8 bytes
            byte[] utf16Bytes = Encoding.Unicode.GetBytes(textWithAllowUnicode);
            byte[] utf8Bytes = Encoding.Convert(Encoding.Unicode, Encoding.UTF8, utf16Bytes);

            // Fill UTF8 bytes inside UTF8 string
            for (int i = 0; i < utf8Bytes.Length; i++)
            {
                // Because char always saves 2 bytes, fill char with 0
                byte[] utf8Container = new byte[2] { utf8Bytes[i], 0 };
                utf8String += BitConverter.ToChar(utf8Container, 0);
            }

            // Return UTF8
            return utf8String;
        
    }

    //Convert.ToUInt16(ch)
    //Char.GetNumericValue(ch));
}


public class CodePointIndexer
{

    public CodePointIndexer(string value)
    {
        this.value = value;
        indexMap = BuildCodePointMap(value);
    } 

    public string Value { get { return this.value; } }

    public char[] this[int index]
    { //may throw out-of-range exception
        get
        {
            int codePointIndex = this.indexMap[index];
            char start = value[codePointIndex];
            if (System.Char.IsSurrogate(start))
                return new char[] { start };
            else
                return new char[] { start, value[codePointIndex + 1] };
        } 
    } 

    String value;
    int[] indexMap;

    #region implementation

    static int[] BuildCodePointMap(string source)
    {
        if (source == null) return null;
        if (source.Length < 1) return new int[] { };
        System.Collections.Generic.List<int> list =
            new System.Collections.Generic.List<int>();
        int currectIndex = 0;
        bool surrogateMode = false;
        foreach (char @char in source)
        {
            list.Add(currectIndex);
            if (surrogateMode) continue;
            surrogateMode = System.Char.IsSurrogate(@char);
            currectIndex++;
        } 
        return list.ToArray();
    } 

    #endregion implementation
}