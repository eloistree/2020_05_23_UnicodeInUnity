﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class UnicodeEvent : UnityEvent<UnicodeId>
{

}
[System.Serializable]
public class UnicodeArrayEvent : UnityEvent<UnicodeId[]>
{

}
[System.Serializable]
public class UnicodeAsHexaEvent : UnityEvent<string>
{

}
[System.Serializable]
public class UnicodeAsIntegerEvent : UnityEvent<int>
{

}

public delegate void UnicodeDeleguateEvent(UnicodeId id);

[System.Serializable]
public class UnicodeId {
    public int m_id;
    public UnicodeId()
    {
        m_id = 0;
    }
    public UnicodeId(int id)
    {
        SetAsInteger(id);
    }
    public UnicodeId(string hexa)
    {
        SetAsHexa(hexa);
    }

    public void SetAsHexa(string value) { 

        m_id = Convert.ToInt32(value, 16);
      
    }
    public void SetAsInteger(int value) {
        m_id = value;
    }

    public string GetAsString() {
        return Char.ConvertFromUtf32(m_id);
    }
    public string GetHexaAsString() {
        return string.Format("{0:X4}", m_id);
    }
    public int GetIdAsInteger() { return m_id; }
}
