﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UI_CommunityUnicodeLink : MonoBehaviour
{
    public LoadUnicodeFilterAsString m_filter;
    public string m_rawCommunityPerPersonFolderUrl = "https://raw.githubusercontent.com/EloiStree/OpenMacroInputCommunityMapping/master/UnicodePack/Emoji/PerPerson/";
    public string m_rawCommunityGroupFolderUrl = "https://raw.githubusercontent.com/EloiStree/OpenMacroInputCommunityMapping/master/UnicodePack/Emoji/Group/";
    public string m_gitCommunityPerPersonFolderUrl = "https://github.com/EloiStree/OpenMacroInputCommunityMapping/tree/master/UnicodePack/Emoji/PerPerson/";
    public string m_gitCommunityGroupFolderUrl = "https://github.com/EloiStree/OpenMacroInputCommunityMapping/tree/master/UnicodePack/Emoji/Group/";
    public void LoadFromName(string nameGiven)
    {

        if (m_filter == null)
            return;
        nameGiven = nameGiven.Replace(" ", "_") + ".md";
        m_filter.LoadAndPushUrl(m_rawCommunityPerPersonFolderUrl + nameGiven);
    }
    public void LoadFromGroup(string nameGiven)
    {

        if (m_filter == null)
            return;
        nameGiven = nameGiven.Replace(" ", "_") + ".md";
        m_filter.LoadAndPushUrl(m_rawCommunityGroupFolderUrl + nameGiven);
    }
    public void OpenCommunityLinkPerPerson()
    {
        Application.OpenURL(m_gitCommunityPerPersonFolderUrl);

    }
    public void OpenCommunityLinkGroup()
    {
        Application.OpenURL(m_gitCommunityGroupFolderUrl);

    }

}
