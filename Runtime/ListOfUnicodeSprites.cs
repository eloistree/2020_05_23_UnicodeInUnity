﻿using UnityEngine;

[CreateAssetMenu(fileName = "UnicodeSpriteList", menuName = "Unicode / Unicode Sprite List", order = 1)]
public class ListOfUnicodeSprites : ScriptableObject
{
    public Texture[] m_allSprites;

}