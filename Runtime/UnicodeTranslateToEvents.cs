﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnicodeTranslateToEvents : MonoBehaviour
{
    public UnicodeEvent m_codeAsUnicode;
    public UnicodeAsHexaEvent m_codeAsHexa;
    public UnicodeAsIntegerEvent m_codeAsInteger;

    public void TranslateAndPush(UnicodeId unicode) {

        m_codeAsUnicode.Invoke(unicode);
        m_codeAsHexa.Invoke(unicode.GetHexaAsString());
        m_codeAsInteger.Invoke(unicode.GetIdAsInteger());

    }
}
