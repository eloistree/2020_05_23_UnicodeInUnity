﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UI_EmojiGrid : MonoBehaviour
{
    public UI_EmojiButton [] m_emojiToPush;
    public int m_value = 20;
    public string m_lastValue = "";
    public UnicodeId [] m_imagesInUniCodeFolder = new UnicodeId[0];

    public UnicodeEvent m_onButtonClick;

    public void Awake()
    {
        
        SetAllButtonFrom(0);
        foreach (var item in m_emojiToPush)
        {
            item.AddListener(UnicodeChoosed) ;
        }
    }
    public void OnDestroy()
    {
        foreach (var item in m_emojiToPush)
        {
            item.RemoveListener(UnicodeChoosed);
        }
    }
    public void UnicodeChoosed(UnicodeId id) {
        m_onButtonClick.Invoke(id);
    }

    
    public void SetWith(UnicodeId[] textWithAllowUnicode)
    {
        m_imagesInUniCodeFolder = textWithAllowUnicode;
        SetAllButtonFrom(0);

    }

    public void ReloadNameFromResources(bool andRefresh=true)
    {
        m_imagesInUniCodeFolder = UnicodeUtility.GetAllUnicodeInResources();
       if(andRefresh)
            SetAllButtonFrom(0);
    }

    public void SetAllButtonFrom(float valueInPct)
    {
        SetAllButtonFrom((int)(valueInPct * m_imagesInUniCodeFolder.Length));
    }


    public void NextPage()
    {
        SetAllButtonFrom(m_value + m_emojiToPush.Length);
    }
    public void PreviousPage()
    {
        SetAllButtonFrom(m_value - m_emojiToPush.Length);
    }

    private void SetAllButtonFrom(int value)
    {
     
        m_value = value;
        foreach (var item in m_emojiToPush)
        {
            string v = "";
            if (m_imagesInUniCodeFolder != null && value>=0 && value < m_imagesInUniCodeFolder.Length)
            {
                v = m_imagesInUniCodeFolder[value].GetHexaAsString();
                item.SetFromHexa(v);
            }
            else item.Clear();
            value++;
        }

    }

    private void OnValidate()
    {
        SetAllButtonFrom(m_value);
        ReloadNameFromResources();
    }
    private void Reset()
    {
        ReloadNameFromResources();
    }



}
