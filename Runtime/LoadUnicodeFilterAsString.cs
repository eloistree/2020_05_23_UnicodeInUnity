﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class LoadUnicodeFilterAsString : MonoBehaviour
{
    public bool m_useWeb=true;
    public string m_webUrl;
    public bool m_useFile=true;
    public TextAsset m_textFile;
    public UnicodeArrayEvent m_loadedUnicode;

    IEnumerator Start()
    {
        if (m_useFile) { 
            m_loadedUnicode.Invoke(UnicodeUtility.GetUnicodesFromString(m_textFile.text));
        }
        if (m_useWeb) {
            WWW w = new WWW(m_webUrl);
            yield return w;
            if (w.error != null && w.error.Length > 0)
            {
                //Debug.Log(">e>" + w.error);
            }
            else {
                //Debug.Log(">v>" + w.text);
                m_loadedUnicode.Invoke(UnicodeUtility.GetUnicodesFromString(w.text));

            }
        }
        yield break;
    }

    public void LoadAndPushUrl(string url)
    {
        StartCoroutine(CoroutineLoadAndPushUrl(url));
    }
    public IEnumerator CoroutineLoadAndPushUrl(string url)
    {
        WWW w = new WWW(url);
        yield return w;
        if (w.error != null && w.error.Length > 0)
        {
        }
        else
        {
            m_loadedUnicode.Invoke(UnicodeUtility.GetUnicodesFromString(w.text));

        }
        yield break;
    }

}
